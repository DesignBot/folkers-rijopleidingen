'use strict'

const path                  = require('path'),
      UglifyJsPlugin        = require('uglifyjs-webpack-plugin'),
      WebpackNotifierPlugin = require('webpack-notifier')

module.exports = {
    mode: 'production', // production/development
    entry: ['./src/js/main.js', './src/scss/main.scss'],
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, './dist/js')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].css',
                            outputPath: '../css'
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.(jpg|png|webp)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 25000,
                        name: '../img/[hash].[ext]'
                    },
                },
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: '../fonts/[hash].[ext]',
                        mimetype: 'application/font-woff'
                    },
                },
            }, 
            {
                test: /\.(ttf|otf|eot|svg)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: '../fonts',
                    },
                }],
            },
        ]
    },

    plugins: [
        new WebpackNotifierPlugin({alwaysNotify: true}),
    ]
}
